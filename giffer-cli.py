#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Antonio Vaccarino

__author__ = 'Antonio Vaccarino'
__docformat__ = 'restructuredtext en'

import os
import argparse

parser = argparse.ArgumentParser(description='Convert video files to animated gifs.')

parser.add_argument('filename', help="Path of the video file")
parser.add_argument('--duration', help="Duration in secs or timecode", default=10)

parser.add_argument('--offset', default="0", help="Seconds (or timecode) to start from into the video")

#adds flag for low quality
parser.add_argument('--lq', default=False, action="store_true", help="Set quality to low to save space (defaults on high)")
parser.add_argument('--framerate', default=4, type=int, help="Number of frames per second")
parser.add_argument('--size', nargs=1, default=["426x240",], help="Image size expressed as widthxheight")
parser.add_argument('--text', nargs=1, type=str, default=None, help="Subtitle to be displayd, allows for \\n")
parser.add_argument('--color', nargs=1, type=str, default="white", help="Name of the color for the subtitle (defaults to \"white\")")
parser.add_argument('--out', nargs=1, type=str, default=["animated.gif",], help="Name of the output file")
parser.add_argument('--slow', nargs=1, default=[1,], help="slowdown effect compared to real time, needs to be > 0, speeds up when > 1");
#not really useful, keeping for future use
#parser.add_argument('--ccout', default=False, action="store_true", help="set if the caption is outside the frame (defaults on inside as  true cc)")
args = parser.parse_args()

print "ARGUMENTS were "
print args

cmds = []

if args.lq:
	tempext = "gif"
else:
	tempext = "png"
tempdir = "giffertemp"
outfilepattern = tempdir+"/out%04d."+tempext
imagesize = args.size[0]
colorname = args.color[0]
slowdown = float(args.slow[0])

try:
	offset = int(args.offset)
except ValueError:
	offset_pre = args.offset.split(":")
	offset_secs = float(offset_pre[-1])
	try:
		offset_mins = int(offset_pre[-2])
	except:
		offset_mins = 0
	try:
		offset_hours = int(offset_pre[-3])
	except:
		offset_hours = 0
	offset = offset_secs + offset_mins*60 + offset_hours*3600

# print "Starting at "+args.offset+" -> ",offset

avconv_args = {
	"offset": offset,
	"infile": args.filename,
	"duration": args.duration,
	"imagesize": imagesize,
	"framerate": args.framerate,
	"outfile": outfilepattern,
}


cmds.append('mkdir '+tempdir)

string_avconv = "avconv -loglevel warning -ss %(offset)s -i %(infile)s -t %(duration)s -vsync 1 -r %(framerate)s -s %(imagesize)s %(outfile)s" % avconv_args
cmds.append(string_avconv)

ingifpattern = tempdir+"/out*."+tempext
outgifname = args.out[0]

convert_assemble_args = {
	"giflist": ingifpattern,
	"outfilename": outgifname,
	"framedelay": (100/args.framerate)/slowdown
}

string_gif_assemble = "convert -loop 0 -delay %(framedelay)s %(giflist)s %(outfilename)s " % convert_assemble_args
cmds.append(string_gif_assemble)

imsize = imagesize.split("x")
im_w = int(imsize[0])
im_h = int(imsize[1])
ratio = float(im_w)/im_h

if ratio > 1.5:
	fontsize = im_h / 10
else:
	fontsize = im_h / 12

if args.text:
	convert_caption_args = {
		"outgifname": outgifname,
		"subtitle": args.text[0],
		"fontsize": fontsize,
		"filler": colorname
	}
	string_gif_label = "convert %(outgifname)s -fill %(filler)s -pointsize %(fontsize)s -undercolor '#00000080' -gravity South -annotate +0+5 \"%(subtitle)s\" %(outgifname)s" % convert_caption_args
	cmds.append(string_gif_label)

#optimization
# string_gif_optimize = "convert -layers Optimize %s %s" % (outgifname, outgifname)
# cmds.append(string_gif_optimize)

cmds.append('rm -f '+tempdir+"/*")

# print "Commands list"
# print cmds

for cmd in cmds:
	print "CMD: "+cmd
	os.system(cmd)

print "DONE!"

